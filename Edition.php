<?php
namespace Core\Oklahoman;
require_once('classes/module/Module_Rel.php');
use \Core\Db as DB;

class Edition {

    public $edition_id;
    public $title;
    public $publisher;
    public $issue_date;
    public $online_pubdate;
    public $cover_art_id;
    public $volume;
    public $issue;
    public $pdf_doc_id;
    public $link_1;
    public $link_2;
    public $dark_date;
    public $details;
    public $site_sid;
    public $version=0;
    
    private $_updateVersion = TRUE;

    const WEATHER_TAG_ID    = 472;
    const DEATH_TAG_ID      = 1171;
    const CLASSIFIED_TAG_ID = 2021;
    const SPORTS_STATS_ID   = 2022;
    const GARAGE_SALE_ID    = 2024;
    
    /**
     *
     * @var string Table Name
     */
    const TABLE_NAME = 'editions';

    /**
     *
     * @var string Database Name
     */
    const DB_NAME = 'newsok7';

    
    public function setUpdateVersion($updateVersion=TRUE)
    {
        $this->_updateVersion = $updateVersion;
    }
    
    /**
     * @param $module_id
     * @param $module_type_id
     * @return $this
     * @throws \Exception
     */
    public function removeRelation($module_id, $module_type_id) {
        $db_write = DB::getInstance()->getConnection(self::DB_NAME, DB::TYPE_WRITE);

        if (empty($this->edition_id)) {
            throw new \Exception("Current Object is not a saved edition");
        }
        
        \Module_Rel::_UnRelate($this->edition_id, EDITION_MODULE_TYPE_ID, $module_id, $module_type_id, true);

        $this->incrementVersion();
        
        return $this;
    }

    /**
     * @param     $module_id
     * @param     $module_type_id
     * @param int $order
     * @return $this
     * @throws DB\ConnectException
     * @throws \Exception
     */
    public function addRelation($module_id, $module_type_id, $order = 999) {
        if (empty($this->edition_id)) {
            throw new \Exception("Current Object is not a saved edition");
        }
        
        \Module_Rel::_Relate($this->edition_id, EDITION_MODULE_TYPE_ID, $module_id, $module_type_id, true, $order);

        $this->incrementVersion();

        return $this;
    }


    /**
     * Caution: Removes all edition relations for the module excluding current edition
     * To avoid articles linking to multiple editions, the function needs to be called right before the module is related to an edition
     * @param     $module_id
     * @param     $module_type_id
     * @return $this
     * @throws DB\ConnectException
     * @throws \Exception
     */
    public function remRelation($module_id, $module_type_id) {
        if (empty($this->edition_id)) {
            throw new \Exception("Current Object is not a saved edition");
        }

        // Type cast these before checks
        $module_id          = intval($module_id);
        $module_type_id     = intval($module_type_id);

        if ($module_id == 0)           throw new \Exception("Need an ID to remove module.");
        if ($module_type_id == 0)      throw new \Exception("Need a Module Type to remove module.");

        $db_write = \Core\Db::getInstance()->getConnection('newsok7', \Core\Db::TYPE_WRITE);

        $sql = "DELETE FROM module_rels WHERE mod_parent_id = $module_id and mod_parent_type_id = $module_type_id 
                    and mod_child_type_id = ".EDITION_MODULE_TYPE_ID." and  mod_child_id != ".$this->edition_id;
        $removed = $db_write->query($sql);

        if(!$removed){
            throw new \Exception("Error removing editions from module: " . $module_id . ", module_type: " . $module_type_id);
        }

        $sql = "DELETE FROM module_rels WHERE mod_child_id = $module_id and mod_child_type_id = $module_type_id 
            and mod_parent_type_id = " . EDITION_MODULE_TYPE_ID." and  mod_parent_id != ".$this->edition_id;
        $removed = $db_write->query($sql);

        if(!$removed){
            throw new \Exception("Error removing module: " . $module_id . ", module_type: " . $module_type_id . " relation from editions");
        }
        return $removed;

    }

    /**
     * Remove a section from the details
     *
     * @param int    $sectionId
     * @param string $sectionTag
     * @return $this
     * @throws \Exception
     */
    function removeSectionTag($sectionId = 0, $sectionTag = '') {
        $sectionId  = (int)$sectionId;
        $sectionTag = strtolower($sectionTag);

        if (empty($sectionId) && empty($sectionTag)) {
            throw new \Exception('invalid params');

        } else if ($sectionId) {

        } else if ($sectionTag) {
            $sections = self::getSectionList();
            if (!isSet($sections[ $sectionTag ])) {
                throw new \Exception('Tag ' . $sectionTag . ' not found');
            }

            $sectionId = $sections[ $sectionTag ]->id;
        }

        $sectionDetails = array();
        forEach ($this->details->sections as $currentSection) {
            if ($sectionId != $currentSection->id) {
                $sectionDetails[] = $currentSection;
            }
        }

        $this->details->sections = $sectionDetails;

        return $this;
    }

    /**
     * Add a section to the sections' details
     * @param     $sectionTag
     * @param int $defaultOrder
     * @throws \Exception
     * @return $this
     */
    function addSectionTag($sectionTag, $defaultOrder = 99999) {
        $sectionTag = strtolower($sectionTag);
        if ($sectionTag === 'deaths') { //No more death
            return $this;
        }

        $sections   = self::getSectionList();

        /**
         * When changing order,
         * verify Extra Sections order @ http://admin.newsok.com/oklahoman-extra
         */
        static $orders = array(
          'news'        => 100,
          'business'    => 200,
          'opinion'     => 300,
          'sports'      => 400,
          'real_estate' => 500,
          'features'    => 600,
          'obit'       =>  700,
          'comics'      => 900
        );

        if (!isSet($sections[ $sectionTag ])) {
            throw new \Exception('Tag ' . $sectionTag . ' not found');
        }

        $section        = $sections[ $sectionTag ];
        $section->order = isset($orders[ $sectionTag ]) ? $orders[ $sectionTag ] : $defaultOrder;

        $found = false;
        if(!empty($this->details->sections)){
            forEach ($this->details->sections as $currentSection) {
                if ($section->id == $currentSection->id) {
                    $found = true;
                    break;
                }
            }
        }
        if (!$found) {
            $this->details->sections[] = $section;
        }

        return $this;
    }

    /**
     * Create edition with following defaults
     *
     * @param      $issueDate
     * @param      $siteSid
     * @param      $title
     * @param      $publisher
     * @param int  $issue
     * @param      $link1
     * @param      $link2
     * @param      $cover_id
     * @param      $pdfId
     * @param      $volume
     * @param      $onlineDate
     * @param null $darkDate
     * @return $this
     */
    public function createEdition($issueDate, $siteSid, $title, $publisher, $issue = 1, $link1, $link2, $cover_id=0, $pdfId=0, $volume, $onlineDate='0000-00-00 00:00:00', $darkDate = null, $version =0) {
        $this->issue_date     = $issueDate;
        $this->publisher      = $publisher;
        $this->title          = $title;
        $this->online_pubdate = $onlineDate;
        $this->dark_date      = $darkDate;
        $this->issue          = $issue;
        $this->link_1         = $link1;
        $this->link_2         = $link2;
        $this->cover_art_id   = $cover_id;
        $this->pdf_doc_id     = $pdfId;
        $this->site_sid       = $siteSid;
        $this->volume         = $volume;
        $this->version        = $version;
        $this->createDefaultDetails();

        $this->save();

        return $this;
    }
    
    public  function incrementVersion()
    {
        $version = 0;
        $db_write = DB::getInstance()->getConnection(self::DB_NAME, DB::TYPE_WRITE);
        
        if(empty($this->edition_id)){
            return $version;
        }
        
        if (!$this->_updateVersion){
            return $this->version;
        }
        
        $version = $this->version + 1;
        
        $sql = "UPDATE ".self::TABLE_NAME." SET version=? WHERE edition_id=?;";
        $stm = $db_write->prepare($sql) or trigger_error($db_write->error . "[$sql]", E_USER_WARNING);

        if (!$stm) {
            error_log(print_r($db_write->error, true));
        }

        $stm->bind_param('ii', $version, $this->edition_id);
        $stm->execute();
        return $version;
        
    }
    
    public  function publishEdition()
    {
        $onlinePubDate = '0000-00-00 00:00:00';
        $db_write = DB::getInstance()->getConnection(self::DB_NAME, DB::TYPE_WRITE);
    
        if(empty($this->edition_id)){
            return $onlinePubDate ;
        }
        
        $onlinePubDate = \Core\Db::escape($this->online_pubdate);
    
        $sql = "UPDATE ".self::TABLE_NAME." SET online_pubdate=? WHERE edition_id=?;";
        $stm = $db_write->prepare($sql) or trigger_error($db_write->error . "[$sql]", E_USER_WARNING);
    
        if (!$stm) {
            error_log(print_r($db_write->error, true));
        }
    
        $stm->bind_param('si', $onlinePubDate, $this->edition_id);
        $stm->execute();
        $this->incrementVersion();
        return $onlinePubDate;
    
    }
    

    /**
     * Save current edition
     *
     * @return $this
     * @throws DB\ConnectException
     */
    public function save() {
        if(empty($this->online_pubdate)){
            throw new \Exception('Missing Publication date');
        }
        $db_write = DB::getInstance()->getConnection(self::DB_NAME, DB::TYPE_WRITE);

        if (empty($this->edition_id)) {
            $edition_id = 'null';
        } else {
            $edition_id = DB::escape($this->edition_id);
        }
        $version = (int) $this->incrementVersion();
        
        $title          = DB::escape($this->title);
        $publisher      = DB::escape($this->publisher);
        $issue_date     = DB::escape($this->issue_date);
        $online_pubdate = DB::escape($this->online_pubdate);
        $cover_art_id   = (int) $this->cover_art_id;
        $volume         = DB::escape($this->volume);
        $issue          = DB::escape($this->issue);
        $pdf_doc_id     = (int) $this->pdf_doc_id;
        $link_1         = DB::escape($this->link_1);
        $link_2         = DB::escape($this->link_2);
        $dark_date      = DB::escape($this->dark_date);
        $details        = DB::escape(json_encode($this->details));
        $site_sid       = DB::escape($this->site_sid);

        $sql = "
          INSERT INTO newsok7.editions (
              edition_id, issue_date,title, publisher, 
              online_pubdate, dark_date, cover_art_id, 
              pdf_doc_id, volume, issue, link_1, link_2, 
              site_sid, details, version
          ) VALUES ( 
            $edition_id, '$issue_date', '$title', '$publisher', 
            '$online_pubdate', '$dark_date', '$cover_art_id',
            '$pdf_doc_id', '$volume', '$issue', '$link_1', '$link_2',
            '$site_sid', '$details', $version
          )
          ON DUPLICATE KEY UPDATE 
            edition_id = LAST_INSERT_ID(edition_id),
            issue_date = '$issue_date',
            title = '$title',
            publisher = '$publisher', 
            online_pubdate = '$online_pubdate', 
            dark_date = '$dark_date', 
            cover_art_id = $cover_art_id,
            pdf_doc_id = $pdf_doc_id, 
            link_1 = '$link_1', 
            link_2 = '$link_2',
            details = '$details',
            version =  $version";

        $db_write->query($sql);

        $this->edition_id = $db_write->last_insert_id();

        return $this;
    }

    /**
     *  Create the default details of the edition
     */
    protected function createDefaultDetails() {
        $this->details           = new \stdclass;
        $this->details->jd       = array(
          (object)array('id' => self::WEATHER_TAG_ID,   'order' => 1300, 'title' => 'Weather'),
          (object)array('id' => self::CLASSIFIED_TAG_ID,'order' => 1400, 'title' => 'Classifieds'),
          (object)array('id' => self::SPORTS_STATS_ID,  'order' => 1500, 'title' => 'Sports Stats'),
          (object)array('id' => self::GARAGE_SALE_ID,   'order' => 1600, 'title' => 'Garage Sale')
        );
        $this->details->sections = array();
    }

    /**
     * Set the current edition values to the $row
     *
     * @param $row
     */
    public function loadRow($row) {
        $properties = (array)$row;
        foreach ($properties as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }

        $this->details = json_decode($this->details);

        if (empty($this->details)) {
            $this->createDefaultDetails();
        }
    }

    /**
     * Load edition from the passed $params
     *
     * @param $params
     * @return $this
     * @throws DB\ConnectException
     * @throws \Exception
     */
    public function loadEdition($params) {
        $db = DB::getInstance()->getConnection(self::DB_NAME);

        if (empty($params['id']) && (empty($params['runDate']) || empty($params['siteSid']))) {
            throw new \Exception('Invalid params for loading edition.');
        }

        if (!empty($params['id'])) {
            $id       = (int)$params['id'];
            $whereSQL = " where edition_id=$id";
        } else {
            $runDate  = DB::escape($params['runDate']);
            $siteSid  = DB::escape($params['siteSid']);
            $whereSQL = " where issue_date = '{$runDate}' AND site_sid='{$siteSid}'";
        }

        $sql    = "SELECT * FROM " . self::TABLE_NAME . $whereSQL;
        $result = $db->query($sql)->fetch_object();
        $this->loadRow($result);

        return $this;
    }

    /**
     * Get a list of all the sections
     *
     * @return array
     * @throws DB\ConnectException
     */
    private function getSectionList() {
        static $_sections = null;

        if ($_sections === null) {
            $db        = DB::getInstance()->getConnection(self::DB_NAME);
            $sql       = "select section_id as id, section, title from sections";
            $rows      = $db->fetchAll($sql);
            $_sections = array();
            foreach ($rows as $row) {
                $_sections[ strtolower($row->section) ] = $row;
            }
        }

        return $_sections;
    }

    static function sectionArticleCount($section, $date, $siteSid='oklahoman') {
        $db = \Core\Db::getInstance()->getConnection('newsok7', \Core\Db::TYPE_READ);

        $siteSid = \Core\Db::escape($siteSid);
        $date = \Core\Db::escape($date);
        $section = \Core\Db::escape($section);

        $sql = "SELECT
                    count(*) as count
                FROM
                    module_rels mr, editions, section_rels sr, sections s
                WHERE
                    mod_parent_id = edition_id and mod_parent_type_id = 56
                AND module_id=mod_child_id and module_type_id=mod_child_type_id
                AND s.section_id=sr.section_id
                AND site_sid = '$siteSid'
                AND issue_date='$date'
                AND s.section='$section'
        ";

        $row = $db->fetchOne($sql);

        return $row ? (int)$row->count : 0;
    }
}
