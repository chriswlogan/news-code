<?php
namespace Core\Oklahoman;

/**
 *  Interface holds the constants of different app 
 * @author mdeepak
 *
 */
interface ProductsInterface
{
    
    const PRINT_REPLICA         = 1;
    
    const ARCHIVES              = 2;
    
    const MOBILE_APP            = 3;
    
    const PAYWALL               = 4;
    
    const OKLAHOMAN_COM         = 5;
    
    const OKLAHOMAN_RADIO       = 6;
    
    const AMZ_ONEDAY_ACCESS     = 7;
}

?>