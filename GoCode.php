<?php
namespace Core\Oklahoman;

class GoCode
{

    /**
     *
     * @var string Table Name
     */
    const TABLE_NAME = 'oklahoman_gocode';

    /**
     *
     * @var string Database Name
     */
    const DB_NAME = 'apps07';

    /**
     * Get the current access code
     * @param string $link
     * @throws \Exception
     */
    public static function getCode($link = '')
    {
        if (empty($link)) {
            throw new \Exception("Missing link to generate code");
        }
        
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME);
        $sql = "SELECT access_code FROM " . self::TABLE_NAME . " WHERE url= ?";
        
        $stmt = $db->prepare($sql);
        $stmt->bind_param('s',$link);
        $stmt->execute();
        $stmt->bind_result($access_code);
        while($stmt->fetch()){
            return $access_code;
        }
        
        return false;
    }
    
    /**
     * Get the url by access code
     * @param string $accessHash
     * @throws \Exception
     */
    public static function getCodeByHash($accessHash = '')
    {
        if (empty($accessHash)) {
            throw new \Exception("Missing $accessHash to generate code");
        }
    
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME);
        $sql = "SELECT url FROM " . self::TABLE_NAME . " WHERE access_code = ?";
        
        $stmt = $db->prepare($sql);
        $stmt->bind_param('s',$accessHash);
        $stmt->execute();
        $stmt->bind_result($url);
        while($stmt->fetch()){
            return $url;
        }
    
        return false;
    }

    /**
     * Generates a new access code and assosciates with the link
     * @param string $link
     * @throws \Exception
     * @return string
     */
    public static function generateCode($link = '')
    {
        if (empty($link)) {
            throw new \Exception("Missing link to generate code");
        }
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME, \Core\Db::TYPE_WRITE);
        
        $sql = "SELECT access_code FROM " . self::TABLE_NAME . " WHERE url='' ORDER BY RAND() LIMIT 1";
        $rows = $db->query($sql)->fetch_object();
        $accessCode = $rows->access_code;
        
        $sql = "UPDATE " . self::TABLE_NAME . " SET url='{$link}' WHERE access_code= ?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param('s',$accessCode);
        $stmt->execute();

        return $accessCode;
    }

    public static function recordArticleId($moduleId, $moduleTypeId, $code) {
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME, \Core\Db::TYPE_WRITE);

        $sql = "INSERT INTO gocode_articles (module_id, module_type_id, access_code) VALUES (?, ?, ?) "
            . "ON DUPLICATE KEY UPDATE access_code=?";
        $stmt = $db->prepare($sql);
        $stmt->bind_param('iis' . 's',
            $moduleId,
            $moduleTypeId,
            $code,

            $code
        );
        $stmt->execute();
    }
}
