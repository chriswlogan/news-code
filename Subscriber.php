<?php
namespace Core\Oklahoman;

use \Core\Db as DB;


class Subscriber
{

    /**
     *
     * @var string Table Name
     */
    const TABLE_NAME = 'oklahoman_subscribers';
    
    /**
     *
     * @var string Database Name
     */
    const DB_NAME = 'peeps07';
    
    public static function LoadByParams($params)
    {
        $db = DB::getInstance()->getConnection(self::DB_NAME);
        
        $wheres = array();
        
        $results = array();
        if(!empty($params['subscriber_id'])){
            $subscriber_id = (int) $params['subscriber_id'];
            $wheres[] = "subscriber_id = $subscriber_id";
        }
        
        if (! empty($wheres)) {
            $whereSql = " WHERE " . implode(" AND ", $wheres);
        }
        
        if(empty($whereSql)){
            return false;
        }
        $orderSql = ' order by id desc ';
        
        $sql = "SELECT * FROM " . self::TABLE_NAME . $whereSql . $orderSql;
        $resultSet = $db->query($sql);
        while($row = $resultSet->fetch_object()) {
            $results[] = new SubscriberObj($row);
        }
        
        return $results;
    }
    
    public static function addPurchaseRecord($profileId, $subscriber_id, $billingRecord = '', $plan_id = 1, $expireDate, $status = 'p')
    {
        if(!$profileId) return false;
        $db = \Core\Db::getInstance()->getConnection('peeps07', \Core\Db::TYPE_WRITE);
        $sql = "INSERT INTO prch_purchase (profile_id, plan_id, cs_id, expire_date, billing_record, "
            . "zip, exp_month, exp_year, user_agent,status) VALUES ({$profileId}, "
            . "?,?,'$expireDate',?,?,?,?,?,?) "
            . "ON DUPLICATE KEY UPDATE plan_id=?, cs_id=?, expire_date='$expireDate', "
            . "billing_record=?, zip=?, exp_month=?, exp_year=?, created=NOW(), user_agent=?,status=?";
    
            $stmt = $db->prepare($sql) or trigger_error($db->error . "[$sql]", E_USER_WARNING);
    
            if (! $stmt) {
                error_log($db->error . "---" . $sql);
            }
            $expireMonth = '';
            $expireYear = '';
            $user_agent="";
            $zip = '';
            try{
                $stmt->bind_param("isssiiss" . "isssiiss",
                    $plan_id,
                    $subscriber_id,
                    $billingRecord,
                    $zip,
                    $expireMonth,
                    $expireYear,
                    $user_agent,
                    $status,
                    $plan_id,
                    $subscriber_id,
                    $billingRecord,
                    $zip,
                    $expireMonth,
                    $expireYear,
                    $user_agent,
                    $status
                    );
    
                $st = $stmt->execute();
                if(!$st){
                    error_log("Error: %s.\n", $stmt->error);
                    return false;
                }
            }catch(Exception $e){
                error_log($e->getMessage());
                return false;
            }
            return true;
    }
}

