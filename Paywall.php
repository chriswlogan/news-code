<?php

namespace Core\Oklahoman;

class Paywall
{
    private $user;
    private $data;
    private $expiration_time;
    private $id;
    private $meter;

    private static $open_tags = array();
    private static $referrers = array();
    private $excludeIPs       = array();

    function __construct($profile, array $data)
    {
      $this->user            = null;
      $this->data            = null;
      $this->expiration_time = null;
      $this->id              = null;
      $this->meter           = null;
    }

    public function getData()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    public function isEnforced()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    public function getCurrentRequest()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    public function getID()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    public function handleRequest($module, $module_type)
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function handleNonSubscriber()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function init()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function reset()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function convertId()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function isLoggedInUser()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function isValidSubscriber()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function resetEndTime()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function isIPExcluded()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function isExpired()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function readInputData(array $input)
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function initMeter($input)
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    private function setUser($profile)
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    public static function isRoadblocked($moduleId, $moduleTypeId = ARTICLE_MODULE_TYPE_ID)
    {
      $moduleId     = intval($moduleId);
      $moduleTypeId = intval($moduleTypeId);
      // Is it a feed article?
      if ($moduleTypeId == ARTICLE_FEED_MODULE_TYPE_ID) {
        return false;
      }
	  // Is article embargoed?
      $embargoObjs = \classes\embargo\Embargo::getInstance()->findByModuleAndModuleType($moduleId, $moduleTypeId);
      if (!empty($embargoObjs) && (strtotime($embargoObjs[$moduleId]->_endTime) > strtotime("now"))) {
        return true;
      }
	  $dbRead       = \Iq_Database::getConnection("newsok7", \Iq_Database::DB_READ);
      // Is the article a blog post?
      $sql          = "SELECT COUNT(*) AS `num` FROM `newsok7`.`section_rels` AS `rel` INNER JOIN `newsok7`.`sections` AS `sec` USING (`section_id`)"
                    . " WHERE `rel`.`module_id` = " . $moduleId . " AND `rel`.`module_type_id` = " . $moduleTypeId . " AND `sec`.`section` = 'blog'";
      $result       = $dbRead->query($sql);
      $row          = $result->fetchObject();
      if (intval($row->num) > 0) {
        return false;
      }
	  // Does article have "the oklahoman" tag?
	  $sql          = "SELECT COUNT(*) AS `num` FROM `newsok7`.`section_rels` AS `rel` INNER JOIN `newsok7`.`sections` AS `sec` USING (`section_id`)"
	                . " WHERE `rel`.`module_id` = " . $moduleId . " AND `rel`.`module_type_id` = " . $moduleTypeId . " AND `sec`.`section` = 'the oklahoman'";
	  $result       = $dbRead->query($sql);
	  $row          = $result->fetchObject();
	  if (intval($row->num) > 0) {
		return true;
	  }
	  return false;
    } // isRoadblocked

    public static function isOpen($sections)
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    protected static function tagsAreOpen(array $tags = array())
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    protected static function isFromSocial()
    {
      throw new \Exception(__METHOD__ . ' is not implemented.');
    }

    public static function getArticleAccessHash($moduleId, $moduleTypeId = null)
    {
      if ($moduleTypeId == null) {
        $moduleTypeId = ARTICLE_MODULE_TYPE_ID;
      }
      return md5("okla-" . $moduleTypeId . "-" . $moduleId);
    } // getArticleAccessHash

} // Paywall

