<?php
namespace Core\Oklahoman;

use Core\BaseObject;

class SubscriberObj extends BaseObject
{

    public $id;

    public $subscriber_id;

    public $name;

    public $address;

    public $city;

    public $state;

    public $zip;

    public $house_number;

    public $phone;

    public $subscription;

    public $active;

    public $edition;
    
    public function __construct( $row = null)
    {
        if ($row == null) return;
    
        $properties = (array)$row;
        foreach ($properties as $key => $val) {
            if (property_exists($this, $key)) {
                $this->{$key} = $val;
            }
        }
    }
}

