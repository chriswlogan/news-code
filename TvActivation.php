<?php

namespace Core\Oklahoman;

class TvActivation {
    const DB_NAME = 'apps07';

    public static function activate($profileId, $code) {
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME, \Core\Db::TYPE_WRITE);

        $sql = "SELECT device_id FROM roku_auth WHERE access_code='{$code}'";
        $rows = $db->fetchAll($sql);

        if (empty($rows)) {
            return "Invalid Code";
        } else {
            $deviceId = $rows[0]->device_id;
        }

        $sql = "SELECT p.profile_id FROM peeps07.profiles p "
            . "INNER JOIN peeps07.oklahoman_subscribers s USING(subscriber_id) "
            . "WHERE p.profile_id = {$profileId}";
        $rows = $db->fetchAll($sql);
        if (empty($rows)) {
            return "Invalid Subscription";
        }

        $sql = "UPDATE apps07.roku_auth SET profile_id={$profileId} WHERE device_id='{$deviceId}'";
        $db->query($sql);

        return true;
    }

    public static function getCode($deviceId) {
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME, \Core\Db::TYPE_WRITE);

        $deviceId = $db->quote($deviceId);

        $sql = "SELECT g.access_code FROM apps07.oklahoman_gocode g "
            . "WHERE NOT EXISTS(SELECT * FROM apps07.roku_auth r WHERE r.access_code = g.access_code) "
            . "ORDER BY RAND() LIMIT 1";
        $row = $db->fetchAll($sql);
        $accessCode = $row[0]->access_code;

        $sql = "INSERT INTO apps07.roku_auth (device_id, access_code) VALUES ({$deviceId}, '{$accessCode}') "
            . "ON DUPLICATE KEY UPDATE access_code='{$accessCode}'";
        $db->query($sql);

        return $accessCode;
    }

    public static function checkActivation($deviceId, $regCode) {
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME, \Core\Db::TYPE_WRITE);

        $deviceId = $db->quote($deviceId);
        $regCode = $db->quote($regCode);

        $sql = "SELECT profile_id FROM apps07.roku_auth "
            . "WHERE device_id={$deviceId} AND access_code={$regCode} AND profile_id != ''";
        $row = $db->fetchAll($sql);

        return $row[0]->profile_id;
    }
}
