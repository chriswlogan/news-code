<?php
namespace Core\Oklahoman;

class Promo
{

    /**
     *
     * @var string Table Name
     */
    const TABLE_NAME = 'subscription_promo';

    /**
     *
     * @var string Database Name
     */
    const DB_NAME = 'peeps07';

    /**
     * Get the current access code
     * @param string $link
     * @throws \Exception
     */
    public static function getLinkedSubscriber($promo = '')
    {
        if (empty($promo)) {
            throw new \Exception("Missing promo code");
        }
        
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME);
        $sql = "SELECT subscriber_id FROM " . self::TABLE_NAME . " WHERE promo_code= ? and approved = 1 and profile_id = 0";
        
        $stmt = $db->prepare($sql);
        $stmt->bind_param('s',$promo);
        $stmt->execute();
        $stmt->bind_result($subscriber_id);
        while($stmt->fetch()){
            return $subscriber_id;
        }
        
        return false;
    }
    

    /**
     * Generates a new access code and assosciates with the link
     * @param string $link
     * @throws \Exception
     * @return string
     */
    public static function updateProfile($promo = '', $profileId = 0)
    {
        \Core\Log\Fb::getInstance()->info($profileId,$promo);
        if (empty($promo) || empty($profileId)) {
            throw new \Exception("Error Associating promo to profile");
        }
        $profileId = (int) $profileId;
        
        $db = \Core\Db::getInstance()->getConnection(self::DB_NAME, \Core\Db::TYPE_WRITE);
        
        $sql = "UPDATE " . self::TABLE_NAME . " SET profile_id='{$profileId}' WHERE promo_code= ?";
        \Core\Log\Fb::getInstance()->info($sql);
        $stmt = $db->prepare($sql);
        $stmt->bind_param('s',$promo);
        $stmt->execute();
        
    }
}

?>